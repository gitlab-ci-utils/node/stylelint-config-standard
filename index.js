/* eslint-disable unicorn/no-null -- null required to disable rules */
export default {
    overrides: [
        {
            customSyntax: 'postcss-html',
            files: ['**/*.html', '**/*.handlebars'],
            name: 'Update syntax for HTML and template files'
        }
    ],
    plugins: [
        'stylelint-declaration-strict-value',
        'stylelint-order',
        'stylelint-use-logical-spec'
    ],
    reportDescriptionlessDisables: true,
    reportInvalidScopeDisables: true,
    reportNeedlessDisables: true,
    reportUnscopedDisables: true,
    // Based on https://www.npmjs.com/package/stylelint-config-standard, with some changes
    rules: {
        'alpha-value-notation': [
            'percentage',
            {
                exceptProperties: [
                    'opacity',
                    'fill-opacity',
                    'flood-opacity',
                    'stop-opacity',
                    'stroke-opacity'
                ]
            }
        ],
        'annotation-no-unknown': true,
        'at-rule-allowed-list': null,
        'at-rule-descriptor-no-unknown': true,
        'at-rule-descriptor-value-no-unknown': true,
        'at-rule-disallowed-list': null,
        'at-rule-empty-line-before': [
            'always',
            {
                except: ['blockless-after-same-name-blockless', 'first-nested'],
                ignore: ['after-comment']
            }
        ],
        'at-rule-no-deprecated': true,
        'at-rule-no-unknown': true,
        'at-rule-no-vendor-prefix': true,
        'at-rule-prelude-no-invalid': true,
        'at-rule-property-required-list': null,
        'block-no-empty': true,
        'color-function-notation': 'modern',
        'color-hex-alpha': null,
        'color-hex-length': 'long', // Changed
        'color-named': 'never', // Added
        'color-no-hex': null,
        'color-no-invalid-hex': true,
        'comment-empty-line-before': [
            'always',
            {
                except: ['first-nested'],
                ignore: ['stylelint-commands']
            }
        ],
        'comment-no-empty': true,
        'comment-pattern': null,
        'comment-whitespace-inside': 'always',
        'comment-word-disallowed-list': null,
        'custom-media-pattern': [
            '^([a-z][a-z0-9]*)(-[a-z0-9]+)*$',
            {
                message: (name) =>
                    `Expected custom media query name "${name}" to be kebab-case`
            }
        ],
        'custom-property-empty-line-before': [
            'always',
            {
                except: ['after-custom-property', 'first-nested'],
                ignore: ['after-comment', 'inside-single-line-block']
            }
        ],
        'custom-property-no-missing-var-function': true,
        'custom-property-pattern': [
            '^([a-z][a-z0-9]*)(-[a-z0-9]+)*$',
            {
                message: (name) =>
                    `Expected custom property name "${name}" to be kebab-case`
            }
        ],
        'declaration-block-no-duplicate-custom-properties': true,
        'declaration-block-no-duplicate-properties': [
            true,
            {
                ignore: ['consecutive-duplicates-with-different-values']
            }
        ],
        'declaration-block-no-redundant-longhand-properties': true,
        'declaration-block-no-shorthand-property-overrides': true,
        'declaration-block-single-line-max-declarations': 1,
        'declaration-empty-line-before': [
            'always',
            {
                except: ['after-declaration', 'first-nested'],
                ignore: ['after-comment', 'inside-single-line-block']
            }
        ],
        'declaration-no-important': true, // Added
        'declaration-property-max-values': null,
        'declaration-property-unit-allowed-list': null,
        'declaration-property-unit-disallowed-list': null,
        'declaration-property-value-allowed-list': null,
        'declaration-property-value-disallowed-list': null,
        'declaration-property-value-keyword-no-deprecated': true,
        'declaration-property-value-no-unknown': true,
        'font-family-name-quotes': 'always-where-recommended',
        'font-family-no-duplicate-names': true,
        'font-family-no-missing-generic-family-keyword': true,
        'font-weight-notation': 'named-where-possible', // Added
        'function-allowed-list': null,
        'function-calc-no-unspaced-operator': true,
        'function-disallowed-list': null,
        'function-linear-gradient-no-nonstandard-direction': true,
        'function-name-case': 'lower',
        'function-no-unknown': true,
        'function-url-no-scheme-relative': null,
        'function-url-quotes': 'always',
        'function-url-scheme-allowed-list': null,
        'function-url-scheme-disallowed-list': null,
        'hue-degree-notation': 'angle',
        'import-notation': 'url',
        'keyframe-block-no-duplicate-selectors': true,
        'keyframe-declaration-no-important': true,
        'keyframe-selector-notation': 'percentage', // Changed
        'keyframes-name-pattern': [
            '^([a-z][a-z0-9]*)(-[a-z0-9]+)*$',
            {
                message: (name) =>
                    `Expected keyframe name "${name}" to be kebab-case`
            }
        ],
        'length-zero-no-unit': true,
        'lightness-notation': 'percentage',
        'max-nesting-depth': null,
        'media-feature-name-allowed-list': null,
        'media-feature-name-disallowed-list': null,
        'media-feature-name-no-unknown': true,
        'media-feature-name-no-vendor-prefix': true,
        'media-feature-name-unit-allowed-list': null,
        'media-feature-name-value-allowed-list': null,
        'media-feature-name-value-no-unknown': true,
        'media-feature-range-notation': 'context',
        'media-query-no-invalid': true,
        'named-grid-areas-no-invalid': true,
        'no-descending-specificity': true,
        'no-duplicate-at-import-rules': true,
        'no-duplicate-selectors': true,
        'no-empty-source': true,
        'no-invalid-double-slash-comments': true,
        'no-invalid-position-at-import-rule': true,
        'no-irregular-whitespace': true,
        'no-unknown-animations': true, // Added
        'no-unknown-custom-media': true,
        'no-unknown-custom-properties': true, // Added
        'number-max-precision': 4,
        'property-allowed-list': null,
        'property-disallowed-list': null,
        'property-no-unknown': true,
        'property-no-vendor-prefix': true,
        'rule-empty-line-before': [
            'always-multi-line',
            {
                except: ['first-nested'],
                ignore: ['after-comment']
            }
        ],
        'rule-selector-property-disallowed-list': null,
        'selector-anb-no-unmatchable': true,
        'selector-attribute-name-disallowed-list': null,
        'selector-attribute-operator-allowed-list': null,
        'selector-attribute-operator-disallowed-list': null,
        'selector-attribute-quotes': 'always',
        'selector-class-pattern': [
            '^([a-z][a-z0-9]*)(-[a-z0-9]+)*$',
            {
                message: (selector) =>
                    `Expected class selector "${selector}" to be kebab-case`
            }
        ],
        'selector-combinator-allowed-list': null,
        'selector-combinator-disallowed-list': null,
        'selector-disallowed-list': null,
        'selector-id-pattern': [
            '^([a-z][a-z0-9]*)(-[a-z0-9]+)*$',
            {
                message: (selector) =>
                    `Expected id selector "${selector}" to be kebab-case`
            }
        ],
        'selector-max-attribute': null,
        'selector-max-class': null,
        'selector-max-combinators': null,
        'selector-max-compound-selectors': null,
        'selector-max-id': null,
        'selector-max-pseudo-class': null,
        'selector-max-specificity': null,
        'selector-max-type': null,
        'selector-max-universal': null,
        'selector-nested-pattern': null,
        'selector-no-qualifying-type': null,
        'selector-no-vendor-prefix': true,
        'selector-not-notation': 'complex',
        'selector-pseudo-class-allowed-list': null,
        'selector-pseudo-class-disallowed-list': null,
        'selector-pseudo-class-no-unknown': true,
        'selector-pseudo-element-allowed-list': null,
        'selector-pseudo-element-colon-notation': 'double',
        'selector-pseudo-element-disallowed-list': null,
        'selector-pseudo-element-no-unknown': true,
        'selector-type-case': 'lower',
        'selector-type-no-unknown': [
            true,
            {
                ignore: ['custom-elements']
            }
        ],
        'shorthand-property-no-redundant-values': true,
        'string-no-newline': true,
        'time-min-milliseconds': null,
        'unit-allowed-list': [
            // Changed
            '%',
            `ch`,
            'deg',
            'em',
            'fr',
            'ms',
            'px',
            'rem',
            's',
            'vh',
            `vw`
        ],
        'unit-disallowed-list': null,
        'unit-no-unknown': true,
        'value-keyword-case': 'lower',
        'value-no-vendor-prefix': [
            true,
            {
                // `-webkit-box` is allowed as standard. See https://www.w3.org/TR/css-overflow-3/#webkit-line-clamp
                ignoreValues: ['box']
            }
        ],
        // eslint-disable-next-line sort-keys -- keep plugin rules separate
        'order/properties-alphabetical-order': true,
        // eslint-disable-next-line sort-keys -- keep plugin rules separate
        'liberty/use-logical-spec': 'always',
        'scale-unlimited/declaration-strict-value': [
            '/color$/',
            {
                expandShorthand: true,
                ignoreValues: ['inherit', 'transparent', 'currentColor'],
                recurseLonghand: true
            }
        ]
    }
};
