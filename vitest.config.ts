// eslint-disable-next-line n/file-extension-in-import -- Named export
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    coverage: {
      all: true,
      include: ['**'],
      reporter: ['text', 'html', 'json']
    },
    exclude: ['**/archive/**', '**/dist/**', '**/node_modules/**'],
    outputFile: 'junit.xml',
    reporters: ['verbose', 'junit']
  }
});
