# Changelog

## 20.0.0 (2025-01-25)

### Changed

- BREAKING: Updated to `stylelint@16.14.0` and added the following new rules as errors:
  [`at-rule-descriptor-no-unknown`](https://stylelint.io/user-guide/rules/at-rule-descriptor-no-unknown/),
  [`at-rule-descriptor-value-no-unknown`](https://stylelint.io/user-guide/rules/at-rule-descriptor-value-no-unknown/),
  [`at-rule-no-deprecated`](https://stylelint.io/user-guide/rules/at-rule-no-deprecated/),
  [`at-rule-prelude-no-invalid`](https://stylelint.io/user-guide/rules/at-rule-prelude-no-invalid/),
  [`declaration-property-value-keyword-no-deprecated`](https://stylelint.io/user-guide/rules/declaration-property-value-keyword-no-deprecated/). (#45)

### Fixed

- Updated to latest dependencies, including `postcss-html@1.8.0` and
  `stylelint-declaration-strict-value@1.10.7`.

## 19.0.0 (2024-12-01)

### Changed

- BREAKING: Added rule
  [`media-query-no-invalid`](https://stylelint.io/user-guide/rules/media-query-no-invalid)
  as an error.
- BREAKING: Added [`reportUnscopedDisables: true`](https://stylelint.io/user-guide/configure/#reportunscopeddisables)
  to config.
- BREAKING: Updated `peerDependencies` to `stylelint@^16.11.0`.

### Fixed

- Updated to latest dependencies, including `stylelint-declaration-strict-value@1.10.6`.
- Added `name` to `overrides` with description.
- Audited all rules and added missing rule
  `media-feature-name-unit-allowed-list` as disabled.

### Miscellaneous

- Removed unnecessary CI job overrides with update to templated v37.

## 18.0.1 (2024-07-12)

### Fixed

- Updated to `postcss-html@1.7.0`

### Miscellaneous

- Updated Renovate config to v1.1.0.
- Updated to ESLint v9 and flat config.

## 18.0.0 (2024-04-28)

### Changed

- BREAKING: Added rule [`no-unknown-custom-media`](https://github.com/stylelint/stylelint/blob/main/lib/rules/no-unknown-custom-media/README.md)
  from `stylelint@16.4.0`, which is now the minimum version required.
- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22. (#44)

## 17.0.1 (2024-02-17)

### Fixed

- Updated to `postcss-html@1.6.0`

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#43)

## 17.0.0 (2024-01-06)

### Changed

- BREAKING: Updated config to ESM for compatibility with `stylelint` v16.
  - Moved testing from `jest` to `vitest` for improved compatibility with ESM.
- BREAKING: Added rule 'lightness-notation', set to 'percentage', from
  `stylelint@16.1.0`, which is now the minimum version required.
- BREAKING: Updated rule `scale-unlimited/declaration-strict-value` to allow
  the static values `inherit`, `currentColor` and `transparent`. (#41)

## Fixed

- Updated to latest dependencies, including
  `stylelint-declaration-strict-value@1.10.4`, `stylelint-order@6.0.4`,
  `stylelint-use-logical-spec@5.0.1`.

## 16.0.0 (2023-11-20)

### Changed

- BREAKING: Added plugin
  [`stylelint-use-logical-spec`](https://www.npmjs.com/package/stylelint-use-logical-spec)
  with rule `liberty/use-logical-spec` set to `always`. (#40)
- BREAKING: Added plugin
  [`stylelint-declaration-strict-value`](https://www.npmjs.com/package/stylelint-declaration-strict-value)
  with rule `scale-unlimited/declaration-strict-value` set to require all
  color values to be specified as variables or custom CSS values. (#40)
- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11). Compatible
  with all current and LTS Node releases (^18.12.0 || >=20.0.0). (#38)

### Miscellaneous

- Changed `package.json` `main` to `exports`.

## 15.0.0 (2023-07-23)

### Changed

- BREAKING: Added new rule `media-feature-name-value-no-unknown` from
  `stylelint@15.8.0`, which is now the minimum version required.

## 14.0.0 (2023-05-15)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#36)
- BREAKING: Added new rule `selector-anb-no-unmatchable` from
  `stylelint@15.3.0`. (#34)
- BREAKING: Added new rule `no-unknown-custom-properties` from
  `stylelint@15.4.0`. (#35)
- BREAKING: Updated peerDependencies to `"stylelint": "^15.4.0"` based on rule additions.

### Fixed

- Update all dependencies, including `stylelint-order@6.0.3`.

## 13.0.1 (2023-02-12)

### Fixed

- Removed `indentation` rule left in `override` for `html`/`handlebars`.
  Deprecated in `stylelint` v15. (#33)

## 13.0.0 (2023-02-12)

### Changed

- BREAKING: Upgraded to `stylelint@15.1.0`.
  - Removed formatting rules that are now
    [deprecated in v15](https://stylelint.io/user-guide/rules/#deprecated), and
    removed `stylelint-config-prettier` since no longer required.
  - Added new rule `declaration-property-value-no-unknown`, which replaces
    plugin `stylelint-csstree-validator` (also removed).
- Pinned all dependencies to ensure consistent behavior.

## 12.0.1 (2022-12-14)

### Fixed

- Fixed peerDependencies to `"stylelint": "^14.16.0"` based on rules added in v12.0.0

## 12.0.0 (2022-12-14)

### Changed

- BREAKING: Added new rule `media-feature-range-notation` from `stylelint`
  v14.16.0. (#30)
- BREAKING: Reviewed all existing rules and specified all in configuration
  to be explicit (even where disabled). Enabled existing rules `color-named`,
  `declaration-no-important`, `font-weight-notation`, `no-unknown-animations`. (#29)

### Fixed

- Update all dependencies, including `stylelint-config-prettier@^9.0.4`

## 11.2.1 (2022-10-24)

### Fixed

- Update package repository reference to try to resolve issues pulling package data with [Renovate](https://github.com/renovatebot/renovate).

## 11.2.0 (2022-10-23)

### Changed

- Removed dependency `stylelint-config-standard` and explicitly defined those rules. There is no change to the final rule set. (#28)

## 11.1.0 (2022-10-23)

### Changed

- Updated `unit-allowed-list` to include `ch` and `vw`. (#26)

### Miscellaneous

- Moved project repository to new sub-group and updated references. (#25)
- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#27)

## 11.0.0 (2022-08-14)

### Changed

- BREAKING: Removed `stylelint-config-prettier` from config settings so this config can be used without prettier. Updated the recommended project config to add it to each project's stylelint config as the last `extends` to ensure it overrides conflicting rules if used with prettier. The `stylelint-config-prettier` package is still included as a dependency of this config. (#22)
- BREAKING: Added new rules `annotation-no-unknown` and `keyframe-selector-notation (percentage)` from `stylelint` v14.10.0. (#24)
- BREAKING: Updated to `stylelint-config-standard` v27, which includes several new rules:
  - Added `keyframe-selector-notation` rule
  - Updated to `stylelint-config-recommended` v8.0.0, which added `annotation-no-unknown` rule
- Added `s`, `ms`, and `deg` to `unit-allowed-list`. (#23)

### Fixed

- Updated to `postcss-html@1.5.0`

## 10.0.0 (2022-06-27)

### Changed

- BREAKING: Added `stylelint-config-prettier` to configuration and removed any rules conflicting with `prettier`. (#20)
- BREAKING: Added plugin `stylelint-order` with rules config set to order properties alphabetically. (#21)

### Fixed

- Updated to latest dependencies

## 9.0.0 (2022-06-14)

### Changed

- BREAKING: Updated to `stylelint-config-standard` v26, which includes several new rules (updated to `stylelint-config-recommended` v8.0.0, added `import-notation` rule)
- BREAKING: Added rule [`keyframe-block-no-duplicate-selectors`](https://github.com/stylelint/stylelint/blob/main/lib/rules/keyframe-block-no-duplicate-selectors/README.md), which required updating peer dependency to `"stylelint": "^14.8.0"`. (#16)
- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#17, #18)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added test coverage threshold requirements (#19)

## 8.0.0 (2022-04-17)

### Changed

- BREAKING: Added rule [`selector-not-notation`](https://github.com/stylelint/stylelint/blob/main/lib/rules/selector-not-notation/README.md) set to `complex`. (#15)

### Fixed

- Updated to latest dependencies, including resolving CVE-2021-44906 (dev only)

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#14)

## 7.0.0 (2022-02-08)

### Changed

- BREAKING: Added `font-family-name-quotes` rule (set to `always-where-recommended`) (#13)
- BREAKING: Updated to `stylelint-config-standard` v25, which adds `function-no-unknown` rule (#13)

## 6.0.0 (2021-12-14)

### Changed

- BREAKING: Updated to `stylelint-config-standard` v24, which excludes `opacity` from `alpha-value-notation` rule (rule is percentage, which leaves `opacity` as number)
- BREAKING: Updated to `stylelint-csstree-validator` v2, which includes an improved `css-tree` generator. This can flag some unidentified issues, previously identified issues in different locations, and changes some error messages.

### Fixed

- Updated to latest dependencies

## 5.0.0 (2021-11-09)

### Added

- BREAKING: Added [CSSTree stylelint plugin](https://github.com/csstree/stylelint-validator) for CSS [validation](https://stylelint.io/about/linting/#validators) (#11)

### Fixed

- Fixed error in `rgba` test case (#10)
- Updated to latest dependencies

## 4.1.0 (2021-10-24)

### Changed

- Updated to `postcss-html` v1.1, which allows single line disables (see [documentation](https://github.com/ota-meshi/postcss-html#turning-postcss-off-from-within-your-html)).

## 4.0.0 (2021-10-22)

### Changed

- BREAKING: Updated to `stylelint` v14
  - Added `post-css` to parse `.html` and `.handlebars` syntax
- BREAKING: Update to `stylelint-config-standard` v23

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Move `jest` config to separate file (#9)

## v3.0.1 (2021-08-29)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Integrate [renovate](https://docs.renovatebot.com/) for automatic dependency updates (#6)

## v3.0.0 (2021-05-31)

### Changed

- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#4)

### Fixed

- Updated to latest dependencies

## v2.0.1 (2021-05-09)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Optimize published package to only include the minimum required files (#3)

## v2.0.0 (2021-03-13)

### Changed

- BREAKING: Updated to `stylelint-config-standard` v21.0.0

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to use standard NPM package collection (#2)

## v1.0.0 (2021-02-15)

Initial release
